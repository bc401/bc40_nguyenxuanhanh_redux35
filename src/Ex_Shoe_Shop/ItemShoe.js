import React, { Component } from 'react';
import { connect } from 'react-redux';

 class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="col-3 p-4">
        <div className="card ">
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{price}</p>
            <button
              onClick={() => {
                this.props.handlePushToCart(this.props.item);
              }}
              href="#"
              className="btn btn-primary"
            >
              Add to card
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePushToCart: (shoe) => {
      let action= {
        type: "THEM_SAN_PHAM",
        payload: shoe, 
      };
      dispatch(action);
    }
  }
}
export default connect(null,mapDispatchToProps)(ItemShoe);