import { dataShoe } from "../data_shoe";

let initialValue = {
    listShoe: dataShoe,
    cart: [],
};

export const shoeReducer = (state=initialValue, action) => {
    switch (action.type) {
        case "THEM_SAN_PHAM": {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
            });
            if (index == -1) {
                let newShoe = { ...action.payload, soLuong: 1};
                cloneCart.push(newShoe);
            } else {
                cloneCart[index].soLuong++;
            }
            return {... state, cart: cloneCart};
        }
        case "XOA_SAN_PHAM": {
            let newcart  = state.cart.filter((item)=>{
                return item.id != action.payload;
            });
            return { ...state, cart: newcart}
        }
        default:
            return state;
    }
}